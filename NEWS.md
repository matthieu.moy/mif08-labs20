# Nouvelles du cours

## 10/12 (quinquies), ré-activation du typechecker dans le TP4

Certains étudiants étaient surpris de ne pas avoir d'erreur de typage quand ils écrivaient des cas de tests incorrects. C'était un mécanisme de désactivation du typechecker qui était activé par erreur, c'est corrigé, faites un pull.

## 10/12 (quater), Ajustement de la deadline du TP4

La deadline du TP4 a été un peu assouplie, voir PLANNING.md pour les détails.

## 10/12, TP4 et base de tests (ter)

Une typo s'était glissée dans le commentaire de la fonction `replace_mem`. Ce sont `S[1]`, `S[2]` et `S[3]` qu'il faut utiliser, pas `S[0]`. Faites un `git pull` pour avoir le commentaire corrigé.

## 10/12, TP4 et base de tests (bis)

Il vous manquait encore une information sur la base de tests : `// SKIP TEST EXPECT` permet de ne pas executer `test_expect` sur un test, vous en aurez besoin pour la division par 0 (un test vous est fourni dans le sujet maintenant).

## 10/12, TP4 et base de tests

Il vous manquait une information sur la base de tests :

* `// EXITCODE n` correspond à la sortie attendue par votre compilateur (i.e. est-ce qu'il y a une erreur à la compilation ?)

_ `// EXECCODE n` correspond à la sortie attendue à l'exécution du binaire généré (i.e. est-ce qu'il y a eu une erreur à l'exécution, comme une division par 0 ?)

Ceci est maintenant documenté dans l'énoncé du tp4, section 4.1.2.

## 9/12, petites mises à jour sur le TP4

Quelques mises à jour sur l'énoncé et le squelette du TP4 :

* Certains morceaux de codes comme l'initialisation automatique de variables sont fournis dans le squelette, vous n'avez pas à les coder (mais assurez-vous qu'ils marchent et qu'ils sont correctement testés par votre base de tests). tp4.pdf a été reformulé pour le prendre en compte.

* addInstructionCondJUMP et Condition n'étaient pas vraiment documentés, il y a maintenant des docstring Python sur cette fonction et cette classe. Faites un `git pull`.

* Les `if` imbriqués ne posent pas vraiment de problème. Vérifiez qu'ils marchent, mais ne vous étonnez pas si vous ne voyez pas de piège.

* Quelques fonctions inutiles (visitForFor, visitArray*) vous étaient fournies, elles ont été supprimées du squelette.

* Documenter votre travail dans un README est une bonne pratique, mais le README ne sera pas pris en compte dans la note, pour vous gagner du temps nous vous autorisons à faire entorse aux bonnes pratiques et à ne rien écrire dans le README. Si vous avez une particularité à signaler, ça reste l'endroit où le faire.

* Quelques typos (par exemple `_prog` -> `_current_function`) ont été corrigées dans tp4.pdf et les slides de présentation des TPs.

## 6/11, Petite correction dans le Makefile, QCM CM3 corrigé

Une petite erreur dans notre Makefile empêchait certains étudiants d'exécuter `make tar`. C'est corrigé, faites un `git pull` (un `git commit` avant sera sans doute nécessaire).

Le corrigé du QCM du CM3 est sur TOMUSS.

## 29/10, Passage en distanciel, machines virtuelles pour travailler les TPs

Tous les cours passent en distanciel complet jusqu'au 1er décembre (au moins). Le [PLANNING.md](PLANNING.md) a été mis à jour pour les séances du mois de Novembre.

Pour vous aider avec le travail à distance, nous avons mis en place des machines virtuelles avec les outils nécessaires pour le cours installés. Dans la mesure du possible, utilisez vos machines personnelles et travaillez dessus en local, mais si vous avez des problèmes d'installation vous pouvez utiliser ces VM en dépannage. La documentation se trouve dans le fichier [VM.md](VM.md).


