# WARNING: this is an old page, the current one is here:

https://forge.univ-lyon1.fr/matthieu.moy/mif08-2021/

# Warning: this page is obsolete










# MIF08-labs
Public files for LYON1-FST M1 students (MIF08 course)

![lyon1logo](logo_lyon1_tiny.png)

[La page de l'avancée du cours (fr), avec slides, sujets de TP & TD, ...](./PLANNING.md)

# Get the repository

```
git clone https://forge.univ-lyon1.fr/matthieu.moy/mif08-labs20.git
cd mif08-labs20
```

# Contents


   * TPxx/     : student companion files for MIF labs 2020-21.


# About the target machine

The target machine is RISCV. The directory contains [instructions](https://github.com/lauregonnord/mif08-labs19/blob/master/INSTALL.md) to install a compiler and a simulator.

# Contact

Matthieu Moy, Université Lyon 1, LIP, https://matthieu-moy.fr/

# Contributors

  * Labs (ENSL 2019 version): Laure Gonnord, Matthieu Moy, Ludovic
    Henrio, Marc De Vismes.
  
